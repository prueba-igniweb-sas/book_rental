<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\ReservationsController;


Auth::routes([
    'register' => false, // Registration Routes...
]);

Route::group(['middleware' => ['auth']], function () {

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// routes clients admin 
Route::get('/clients', [ClientsController::class, 'index'])->name('index.clients');
Route::post('clients', [ClientsController::class, 'store'])->name('clients.store');
Route::get('/clients-get', [ClientsController::class, 'get_clients'])->name('get.clients');
Route::post('client-update', [ClientsController::class, 'update_client'])->name('update_client');
Route::get('clients/{id}/edit', [ClientsController::class, 'edit'])->name('clients_edit');
Route::get('client_delete/{id}' , [ClientsController::class, 'destroy_client'])->name('destroy_client');

// routes categories admin 
Route::get('/categories', [CategoriesController::class, 'index'])->name('index_categories');
Route::post('/categories', [CategoriesController::class, 'store'])->name('categories.store');
Route::get('/categories-get', [CategoriesController::class, 'get_category'])->name('get_categories');
Route::get('categories/{id}/edit', [CategoriesController::class, 'edit'])->name('category_edit');
Route::post('category-update', [CategoriesController::class, 'update_category'])->name('update_category');
Route::get('category_delete/{id}' , [CategoriesController::class, 'destroy_category'])->name('destroy_category');

// routes books admin 
Route::get('/books', [BooksController::class, 'index'])->name('books');
Route::post('/books', [BooksController::class, 'store'])->name('books_store');
Route::get('/books-get', [BooksController::class, 'get_books'])->name('get_books');
Route::get('books/{id}/edit', [BooksController::class, 'edit'])->name('books_edit');
Route::post('books-update', [BooksController::class, 'update_book'])->name('update_books');
Route::get('book_delete/{id}' , [BooksController::class, 'destroy_book'])->name('destroy_books');

//routes reservations 
Route::get('/reservations', [ReservationsController::class, 'index'])->name('reservations');
Route::get('/devolution/{id}', [ReservationsController::class, 'book_devolution'])->name('devolution');
Route::post('reservation', [ReservationsController::class, 'book_reservation'])->name('reservation');

});



