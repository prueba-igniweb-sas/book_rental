<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder; 
use Spatie\Permission\Models\Role; 
use Spatie\Permission\Models\Permission; 
use App\Models\User;
use App\Models\Clients;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UsersSeeder extends Seeder
{

    public function run()
    {
        \DB::table('users')->delete();

        $user1 = User::create([
            'email'             => 'admin@admin.com',
            'name'              =>  'Administrador',
            'email_verified_at' =>  '2019-12-02 12:04:46',
            'password'          =>  Hash::make(12345678),
        ]);
        $user1->assignRole('administrator');
    }
}
