<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'clients']);
        Permission::create(['name' => 'all']);
        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'client']);
        $role->givePermissionTo('clients');

        $role = Role::create(['name' => 'administrator']);
        $role->givePermissionTo('all');
    }
}
