@extends('layouts.auth')

@section('css_extra')
<style type="text/css">
	body{
	background:#000;
	background:url("/assets/images/login/fondo.jpg");
	background-size:100% 100%;
	background-attachment:fixed;
	color:#fff;
	font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; 
    font-weight:100;
	-webkit-font-smoothing: antialiased;
	margin:0px;
}
	.form-control-simple{
		padding-left: 2.5rem;
		
	}
	.icon-input-auth{
		font-size: 30px;
    	position: absolute;
    	color: #000;
	}
	form{
		margin: 1rem 0rem;
	}
	.forgotpw{
		font-size: 12px;
	    padding: 0;
	    color: #000;
	}
	.forgotpw:hover{
		 text-decoration: none;
	}
	.form-check-input {
    position: absolute;
    margin-top: 0.1rem;
    margin-left: -1.25rem;
}
	.decoration-login{
		width: 50%
	}
	.card-body{
		padding: 2rem 4rem;
	}
	.fondo{
		background-color:rgba(0,0,0,0.6);
	}
	.logo{
		border-radius: 3%;
	}
	.btn{
		background-color:#fff;
	}
</style>
@endsection

@section('content')

<div style="margin-top: 10%"  class="container">
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div class="card-body fondo" >
					<div align="center">
						<img src="{{asset('assets/images/login/logo.jpg')}}" width="120" height="100" class="logo">
						<br>
						<br>
						<h2>Bienvenido</h2>
					</div>
					<form method="POST" action="{{ route('login') }}">
						@csrf
						<div class="form-group">
							<span class="iconify icon-input-auth" data-icon="carbon:user-avatar-filled-alt"></span>
							<input id="email" class="form-control form-control-simple @error('email') is-invalid @enderror" type="text" placeholder="Correo electrónico" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
							 @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>				
						<div class="form-group">
							<span class="iconify icon-input-auth" data-icon="dashicons:lock"></span>
							<input id="password" class="form-control form-control-simple @error('password') is-invalid @enderror" type="password" placeholder="Contraseña" name="password" required autocomplete="current-password">
							 @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div style="display: flex; justify-content: space-between; padding: 0rem 1.5rem;">
							<div style="font-size: 12px; align-self: self-end;">
								<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
							Recordar para la proxima
							</div>
							{{-- <div>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link forgotpw" href="{{ route('password.request') }}">
                                        ¿Olvidaste tu contraseña?
                                    </a>
                                @endif							
							</div> --}}
						</div>
						<br>
						<div align="center">
							<button style="width: 100%" class="btn btn-primary-custom">Iniciar sesión</button>
						</div>
					</form>
				</div>
			</div>			
		</div>
	</div>
</div>




@endsection