@extends('layouts.auth')
@section('css_extra')
<style type="text/css">
    body{
        background: #eee
    }
    .card{
        background: #fafafa;
        box-shadow: 0px 0px 10px #bdbdbd;
    }
    .card-body{
        padding: 3rem 5rem;
    }   
    .form-control-simple{
        padding-left: 2.5rem
    }
    .icon-input-auth{
        font-size: 30px;
        position: absolute;
        color: #06468a
    }
    form{
        margin: 1rem 0rem
    }
    .forgotpw{
        font-size: 12px;
        padding: 0;
        color: #06468a;
    }
    .forgotpw:hover{
         text-decoration: none;
    }
    .form-check-input {
    position: absolute;
    margin-top: 0.1rem;
    margin-left: -1.25rem;
}
.decoration-login{
    width: 50%
}
.d1{
    background: #06468a;
    height: 5px
}
.d2{
    background: #007040;
    height: 5px
}
</style>
@endsection

@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        ¡Le hemos enviado un correo electrónico con su enlace de restablecimiento de contraseña!
    </div>
@endif
<div style="margin-top: 10%" class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div style="display: flex">
                    <div class="decoration-login d1"></div>
                    <div class="decoration-login d2"></div>
                </div>
                <div class="card-body">
                    <div align="center">
                        <img src="../assets/images/Logo.svg">
                        <br>
                        <br>
                        <h3>Restablece tu contraseña</h3>
                    </div>
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                                <span class="iconify icon-input-auth" data-icon="bx:mail-send"></span>
                                <input id="email" type="email" class="form-control form-control-simple @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Ingrese su correo electrónico">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>No podemos encontrar un usuario con esa dirección de correo electrónico.</strong>
                                    </span>
                                @enderror    
                                <br>         
                        <div align="center">
                            <button type="submit" style="width: 100%" class="btn btn-primary-custom">Restablecer</button>
                        </div>
                    </form>
                </div>
            </div>          
        </div>
    </div>
</div>

    
@endsection
