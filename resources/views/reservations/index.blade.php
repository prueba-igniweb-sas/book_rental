@extends('layouts.base')

@section('css_extra')	
<style>
    #book_create
    {
        background-color:rgba(125, 201, 202 ,0.10);
        border-radius: 20px;
        margin-left: 15px;
    }
    h1{
        color: #492905;
        font-size: 40px;
    }
    #image_book{
        border-radius: 100%;
    }
    td{
        text-align: center;
    }

</style>
@endsection
@section('content')
<br>
<!-- Large modal -->
    <h1>Listado de reservaciones 
        @can('clients')
            <span><a href="{{route('books')}}" class="btn btn-warning">Ir a reservar libros</a></span>
        @endcan
    </h1>
    
    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
        <div class="shadow mb-4">
            <div class="card-body">
                <table id="books_list" class="table table-hover table-codensed" >
                        <thead style="background-color: #492905; color:aliceblue;">
                            <tr>
                                <th style="text-align:center;">Imagen libro</th>
                                <th style="text-align:center;">Nombre libro</th>
                                <th style="text-align:center;">Días de reservación</th>
                                <th style="text-align:center;">Fecha de reservación</th>
                                <th style="text-align:center;">Fecha de devolución</th>
                                <th style="text-align:center;">Estado</th>
                              
                                <th style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>
                        
                            @foreach ($reservations as $reservation)
                            <tbody>
                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                              <div class="card-header" id="headingOne">
                                                <h1 style="text-align: center">{{$reservation->book->name}}</h1>
                                                <h5 class="mb-0">
                                                  <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Datos del libro
                                                  </button>
                                                </h5>
                                              </div>
                                          
                                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">AUTOR: {{$reservation->book->author}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">EDITORIAL: {{$reservation->book->editorial}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">IDIOMA: {{$reservation->book->idiom}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">PUBLICACIÓN: {{$reservation->book->release_date}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>
                                              </div>
                                            </div>
                                            <div class="card">
                                              <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                  <button class="btn btn-info collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    Datos de la reservación
                                                  </button>
                                                </h5>
                                              </div>
                                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">NOMBRE DEL CLIENTE: {{$reservation->client->name}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">CELULAR DEL CLIENTE: {{$reservation->client->phone}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">DÍAS RESERVADO: {{$reservation->rental_days}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">FECHA DE RESERVACIÓN: {{$reservation->rental_date}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">FECHA DE ENTREGA: {{$reservation->date_of_delivery}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">ESTADO: Reservado</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="card">
                                              <div class="card-header" id="headingThree">
                                                <h5 class="mb-0">
                                                  <button class="btn btn-info collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    Resumen del libro
                                                  </button>
                                                </h5>
                                              </div>
                                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">{{$reservation->book->abstract}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                    </div>
                                </div>
                                <td>
                                    <img id="image_book" src="{{asset('storage/book_images/'.$reservation->book->book_image.'')}}" width="50px;" height="50px" alt="">
                                </td>
                                <td>{{$reservation->book->name}}</td>
                                <td>{{$reservation->rental_days}}</td>
                                <td>{{$reservation->rental_date}}</td>
                                <td>{{$reservation->date_of_delivery}}</td>
                                <td>                
                                    <h5><span class="badge badge-warning">Por devolver</span></h5></p>
                                </td>
                                <td>
                                    <a href=""  data-toggle="modal" data-target=".bd-example-modal-lg">Ver</a>
                                    @can('clients')
                                    | <a href="{{route('devolution', $reservation->id)}}">Devolver</a>
                                    @endcan
                                   </td>
                                </tbody>
                            @endforeach
                        
                </table>		
            </div>
        </div>	
    </div>
</div>

@endsection
@section('js_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
// function created of client 
$(function(){
 $('#books_list').DataTable({
    "language": {
                "search": "Buscar reseravación:",
                "lengthMenu": "Mostrando _MENU_ reseraciones por pagína.",
                "zeroRecords": "Upss! Parece que aun no hay ninguna reservacion creada.",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin reservaciones añadidas.",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                }
            }
});
});

</script>



