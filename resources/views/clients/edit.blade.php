<!-- Modal -->
<div class="modal fade edit_client_modal" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="{{route('update_client')}}" method="post" id="client_update">
                    @csrf
                    <input type="hidden" name="client_id" id="client_id">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-6">
                                <label for="">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text name_error"> </span>
                            </div>
                            <div class="col-6">
                                <label for="">Apellido</label>
                                <input type="text" name="lastname" id="lastname" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text lastname_error"> </span>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-6">
                                <label for="">Celular</label>
                                <input type="number" name="phone" id="phone" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger phone_error"></span>
                            </div>
                            <div class="col-6">
                                <label for="">Dirección</label>
                                <input type="text" name="address" id="address" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text address_error"> </span>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-6" style="text-align: -webkit-center;">
                                <label for="">Imagen de perfil</label>
                                <input type="file" name="profile_picture" id="imagen" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text profile_picture_error"> </span>
                            </div>
                            <div class="col-6">
                                <br>
                                <img src="" id="imagen1" alt="" width="60px" height="60px">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>