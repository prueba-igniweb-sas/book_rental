@extends('layouts.base')

@section('css_extra')	
<style>
    #client_create
    {
        background-color:rgba(125, 201, 202 ,0.10);
    }
    h1{
        color: #492905;
        font-size: 40px;
    }
</style>
@endsection
@section('content')
@can('all')

<br>
<div class="title-page">
    <h1>Crear un cliente</h1>
</div>
<!-- Modal -->

@include('clients.edit')
<form action="{{ route('clients.store') }}" id="client_create" method="post" >
    @csrf
    <br>
    <div class="form-row" style="margin-left:15px;">
    <div class="col-3" id="button_image_profile">
        <label for="name">Imagen de perfil</label><br>
        <span class="text-danger error-text profile_picture_error"></span>
        <button type="button" class="browse_preview_profile btn btn-warning"> 
        <i class="fa fa-upload" style="color:#492905" aria-hidden="true"></i> Seleccionar imagen</button>
    </div>
    <div style="text-align: -webkit-center; display:none;" class="col-3" id="image_profile">
        <img src=" {{asset('assets/images/profile_default.png')}} " id="preview_profile_client" class="browse_preview_profile" height="100px" width="100px" style="border-radius: 100%">
        <input type="file" name="profile_picture" class="file">
    </div>     
    <div class="col-3">
        <label for="name">Nombre</label>
        <input type="text" class="form-control" name="name">
        <span class="text-danger error-text name_error"></span>
    </div>
    <div class="col-3">
      <label for="name">Apellido</label>
      <input type="text" class="form-control" name="lastname" >
      <span class="text-danger error-text lastname_error"></span>
    </div> 
    <div class="col-3">
        <label for="name">Correo</label>
        <input type="email" class="form-control" name="email" >
        <span class="text-danger error-text email_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Celular</label>
        <input type="text" class="form-control" name="phone" >
        <span class="text-danger error-text phone_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Dirección</label>
        <input type="text" class="form-control" name="address">
        <span class="text-danger error-text address_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Contraseña</label>
        <input type="password" class="form-control" name="password">
        <span class="text-danger error-text password_error"></span>
    </div>
     
    <div class="col-3">
    <br>
    <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>
<br>
</form>
<br>
<br>
    <h1>Listado de clientes</h1>
    <br>
        <div class="shadow mb-4">
            <div class="card-body">
                <table id="clients_list" class="table table-hover table-codensed">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Imagen</th>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Apellido</th>
                                <th style="text-align:center;">Celular</th>
                                <th style="text-align:center;">Correo</th>
                                <th style="text-align:center;">Dirrección</th>
                                <th style="text-align:center;">Acciones &nbsp&nbsp&nbsp&nbsp&nbsp
                                    <button class="btn btn-sm btn-danger d-none" id="borrarTodo">Borrar todo</button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                </table>		
            </div>
        </div>	
    </div>
</div>
@elsecan('clients')
<div style="background-color:yellow;">
    <h5>No tienes permitido el ingreso a esta pagina, vuelve al inicio </h5>
</div>
   
    
@endcan
@endsection
@section('js_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    // function to preview an image 
    $(function(){
        $(document).on("click", ".browse_preview_profile", function() {
            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
        $('input[class="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            
            var image_profile_block = document.getElementById("image_profile");
            var button_profile_block = document.getElementById("button_image_profile");
            document.getElementById("preview_profile_client").src = e.target.result;

            if (image_profile_block.style.display === "none") {
                image_profile_block.style.display   = "block";
                button_profile_block.style.display  = "none";
            }else{
                image_profile_block.style.display   = "block";
            }
           
        };
        reader.readAsDataURL(this.files[0]);
    });

    });

</script>
 <!--SCRIPTS-->

<script>
  $(document).on("click", ".js_delete", function(){
    var url = "{{url('categories/', '##')}}".replace("##", $(this).data("id"));

    $("#delete_client").attr('action', url);

  });
</script>
{{-- start ajax functions  --}}
<script>
    toastr.options.preventDuplicates = true;

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });

    // function created of client 
    $(function(){
        $('#client_create').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType:'json',
                    beforeSend:function(){
                        $(form).find('span.error-text').text('');
                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $(form)[0].reset();
                            $('#clients_list').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            console.log(1);
                        }
                    }
                });
            });
            // init table 
            $('#clients_list').DataTable({
                language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Clientes",
                "infoEmpty": "Mostrando 0 to 0 of 0 Productos",
                "infoFiltered": "(Filtrado de _MAX_ total Clientes)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Clientes",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                    }
                },
                processing:true,
                info:true,
                ajax:"{{url('clients-get')}}",
                "pageLength":5,
                "aLengthMenu":[[5,10,25,50,-1],[5,10,25,50, "Todos"]],
                
                columns:[
                    {
                        data:'profile_picture', 
                        render:function(data,type, row)
                        {
                            return '<center><img src="storage/profile_clients/' + data +'"width="50" height="50"></center>';
                        }
                    },
                    {data:'name', name:'name'},
                    {data:'lastname', name:'lastname'},
                    {data:'phone', name:'phone'},
                    {
                        data:'users.email', 
                        render:function(data,type, row)
                        {
                            return ''+data+'';
                        }
                    },
                    {data:'address', name:'address'},
                    {data:'Acciones', name:'Acciones', orderable:false, searchable:false},
                ]
                
                // seleccionar solo los registros de una paginacion 
            }).on('draw', function(){
                $('input[name="check_cliente"]').each(function(){this.checked = false;});
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#borrarTodo').addClass('d-none');
            });

            //edit client
            $(document).on('click', '#client_edit', function(){
                $('.edit_client_modal').modal('show');

                var client_id = $(this).data('id');
                $('.edit_client_modal').find('form')[0].reset();
                $('.edit_client_modal').find('span-error-text').text('');
               
                $.get('<?= url("clients/'+client_id+'/edit") ?>',{client_id:client_id}, function(data){
                    $('.edit_client_modal').find('input[name="client_id"]').val(data.edit.id);
                    $('.edit_client_modal').find('input[name="name"]').val(data.edit.name);
                    $('.edit_client_modal').find('input[name="lastname"]').val(data.edit.lastname);
                    $('.edit_client_modal').find('input[name="address"]').val(data.edit.address);
                    $('.edit_client_modal').find('input[name="email"]').val(data.edit.users.email);
                    $('.edit_client_modal').find('input[name="phone"]').val(data.edit.phone);
                    var data_img = data.edit.profile_picture;
                    $('.edit_client_modal #imagen1').attr("src", "storage/profile_clients/"+data_img);
                    $('.edit_client_modal').modal('show');
                },'json');
            });
                // actualizar producto  
                $('#client_update').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData:false,
                    dataType:'json',
                    contentType:false,
                    beforeSend: function(){

                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $('#clients_list').DataTable().ajax.reload(null, true);
                            $('.edit_client_modal').modal('hide');
                            $('.edit_client_modal').find('form')[0].reset();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    }

                })
            });
            // borrrar un solo producto 
            $(document).on('click', '#client_destroy', function(){
                var client_id = $(this).data('id');
                // alert(producto_id);
                Swal.fire({
                    title:'¿Esta seguro?',
                    html:'Usted <b>borrará</b> este cliene de forma permanente ',
                    showCancelButton:true,
                    showCloseButton:true,
                    confirmButtonText:'Si, Borrar',
                    cancelButtonText:'Cancelar',
                    confirmButtonColor:'#556ee6',
                    cancelButtonColor:'#d33',
                    width:300,
                    allowOutsideClick:false
                }).then(function(result){
                    $.get('<?= url("client_delete/'+client_id+'") ?>', {client_id:client_id}, function(data){
                        if (data.code == 1) {
                            $('#clients_list').DataTable().ajax.reload(null, true);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }else{
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    },'json');
                });
            });
            
            // seleccioinar todos los checkbox desde el th 
            $(document).on('click', 'input[name="main_checkbox"]', function(){
                if(this.checked){
                    $('input[name="check_producto"]').each(function(){
                        this.checked = true;
                    });
                }else{
                    $('input[name="check_producto"]').each(function(){
                        this.checked = false;
                    });
                }
                borrartodocheck();
            });

            // seleccioinar todos los checkbox desde el th 
            $(document).on('change', 'input[name="check_producto"]', function(){
                if ($('input[name="check_producto"]').length == $('input[name="check_producto"]:checked').length) {
                    $('input[name="main_checkbox').prop('checked', true);
                } else {
                    $('input[name="main_checkbox"]').prop('checked', false);
                }
                borrartodocheck();
            });
            
            // boton, mostrar  ocultar boton de borrar varios 
            // function borrartodocheck(){
            //     if ($('input[name="check_producto"]:checked').length > 0) {
            //         $('button#borrarTodo').text('Borrar ('+$('input[name="check_producto"]:checked').length+')').removeClass('d-none');
            //     }else{
            //         $('button#borrarTodo').addClass('d-none');
            //     }
            // }

            // borrar registros multiples con checkbox
            // 

        // fin ajax funciones 
        });

        
            
            // fin table 
        // fin ajax funciones 
</script>
