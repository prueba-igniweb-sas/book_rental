@extends('layouts.base')

@section('css_extra')	
<style>
    #category_create
    {
        background-color:rgba(125, 201, 202 ,0.10);
        border-radius: 20px;
        margin-left: 15px;
    }

</style>
@endsection
@section('content')
<br>
<div class="title-page">
    <h1>Crear un cliente</h1>
</div>
<!-- Modal -->

@include('categories.edit')
<form action="{{ route('categories.store') }}" id="category_create" method="post" >
    @csrf
    <br>
    <div class="form-row" style="margin-left:15px; margin-right:15px;">
      <div class="col-4">
          <label for="name">Nombre</label>
          <input type="text" class="form-control" name="name">
          <span class="text-danger error-text name_error"></span>
      </div>
      
      <div class="col-5">
        <label for="name">Descripción</label>
        <textarea name="description" cols="50" rows="5"></textarea>
        <span class="text-danger error-text description_error"></span>
      </div> 
      
    <div class="col-3">
    <br>
    <button type="submit" class="btn btn-secondary">Guardar categoria</button>
    </div>
</div>
<br>
</form>
<br>
<br>
<br>
<div class="title-page">
    <h1>Listado de categorias</h1>
</div>
        <div class="shadow mb-4">
            <div class="card-body">
                <table id="category_list" class="table table-hover table-codensed">
                        <thead>
                            <tr>
                              <th style="text-align:center;">Nombre</th>
                              <th style="text-align:center;">Descripción</th>
                              <th style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                </table>		
            </div>
        </div>	
    </div>
</div>

@endsection
@section('js_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

{{-- start ajax functions  --}}
<script>
    toastr.options.preventDuplicates = true;

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });

    // function created of book
    $(function(){
        $('#category_create').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType:'json',
                    beforeSend:function(){
                        $(form).find('span.error-text').text('');
                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $(form)[0].reset();
                            $('#category_list').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            console.log(1);
                        }
                    }
                });
            });
            // init table 
            $('#category_list').DataTable({
                language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Categorias",
                "infoEmpty": "Mostrando 0 to 0 of 0 Productos",
                "infoFiltered": "(Filtrado de _MAX_ total Categorias)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Categorias",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                    }
                },
                processing:true,
                info:true,
                ajax:"{{route('get_categories')}}",
                "pageLength":5,
                "aLengthMenu":[[5,10,25,50,-1],[5,10,25,50, "Todos"]],
                
                columns:[
                    {data:'name', name:'name'},
                    {data:'description', name:'description'},
                    {data:'Acciones', name:'Acciones', orderable:false, searchable:false},
                ]
                
            // seleccionar solo los registros de una paginacion 
            }).on('draw', function(){
                $('input[name="check_cliente"]').each(function(){this.checked = false;});
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#borrarTodo').addClass('d-none');
            });

            //edit client
            $(document).on('click', '#category_edit', function(){
                $('.edit_category_modal').modal('show');

                var category_id = $(this).data('id');
                $('.edit_category_modal').find('form')[0].reset();
                $('.edit_category_modal').find('span-error-text').text('');
               
                $.get('<?= url("categories/'+category_id+'/edit") ?>',{category_id:category_id}, function(data){
                    $('.edit_category_modal').find('input[name="category_id"]').val(data.edit.id);
                    $('.edit_category_modal').find('input[name="name"]').val(data.edit.name);
                    $('.edit_category_modal').find('textarea[name="description"]').val(data.edit.description);
                    $('.edit_category_modal').modal('show');
                },'json');
            });
                //category update  
                $('#update_category').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData:false,
                    dataType:'json',
                    contentType:false,
                    beforeSend: function(){

                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $('#category_list').DataTable().ajax.reload(null, true);
                            $('.edit_category_modal').modal('hide');
                            $('.edit_category_modal').find('form')[0].reset();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    }

                })
            });
            //delete category
            $(document).on('click', '#category_destroy', function(){
                var category_id = $(this).data('id');
                Swal.fire({
                    title:'¿Esta seguro?',
                    html:'Usted <b>borrará</b> esta categoria y los libros de esta categoria',
                    showCancelButton:true,
                    showCloseButton:true,
                    confirmButtonText:'Si, Borrar',
                    cancelButtonText:'Cancelar',
                    confirmButtonColor:'#556ee6',
                    cancelButtonColor:'#d33',
                    width:300,
                    allowOutsideClick:false
                }).then(function(result){
                    $.get('<?= url("category_delete/'+category_id+'") ?>', {category_id:category_id}, function(data){
                        if (data.code == 1) {
                            $('#category_list').DataTable().ajax.reload(null, true);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }else if (data.code == 2) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                title: data.msg,
                                text: 'No puede eliminar la categoria, pues el libro que la contiene esta alquilado.',
                                showConfirmButton: false,
                                timer: 5000
                            });
                        }else{
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    },'json');
                });
            });
        // fin ajax funciones 
        });

            // fin table 
        // fin ajax funciones 
</script>
   