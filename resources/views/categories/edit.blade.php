<!-- Modal -->
<div class="modal fade edit_category_modal" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Categoria</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="{{route('update_category')}}" method="post" id="update_category">
                    @csrf
                    <input type="hidden" name="category_id" id="category_id">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-12">
                                <label for="">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text name_error"> </span>
                            </div>
                            <div class="col-12">
                                <label for="">Descripción</label>
                                <textarea name="description" id="description" cols="40" rows="10"></textarea>
                                <span class="text-danger error-text description_error"> </span>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>