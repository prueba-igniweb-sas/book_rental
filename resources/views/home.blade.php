@extends('layouts.base')

@section('css_extra')	

@endsection
<!-- Button trigger modal -->
@section('content')

@can('all')
<div class="row">
    <div class="col-sm-3">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title" style="color: #492905;">Clientes <span class="badge badge-warning">{{$clients}}</span></h5></p>
          <br>
          <a href="{{route('index.clients')}}" class="btn btn-secondary">Gestionar clientes</a>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title" style="color: #492905;">Categorias de libros <span class="badge badge-success">{{$categories}}</span></h5></p>
                <br>
                <a href="{{route('index_categories')}}" class="btn btn-secondary">Gestionar categorias</a>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title" style="color: #492905;">Libros <span class="badge badge-primary">{{$books}}</span></h5></p>
                <br>
                <a href="{{route('books')}}" class="btn btn-secondary">Gestionar libros</a>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title" style="color: #492905;">Reservaciones de clientes <span class="badge badge-danger">{{$reservations}}</span></h5></p>
                <br>
                <a href="{{route('reservations')}}" class="btn btn-secondary">Ver reservaciones</a>
            </div>
        </div>
    </div>
</div>    
@endcan
@can('clients')
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title" style="color: #492905;">Libros disponibles <span class="badge badge-primary">{{$c_books}}</span></h5></p>
                <br>
                <a href="{{route('books')}}" class="btn btn-secondary">Ver libros</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title" style="color: #492905;">Mis reservaciones <span class="badge badge-warning">{{$c_reservations}}</span></h5></p>
                <br>
                <a href="{{route('reservations')}}" class="btn btn-secondary">Gestionar reservaciones</a>
            </div>
        </div>
    </div>
</div>    
@endcan

@endsection
