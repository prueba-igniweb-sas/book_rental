<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	{{-- <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/images/logo.svg')}}"> --}}
	<title>Libreria</title>
	<link rel="stylesheet" type="text/css" href=" {{asset('assets/bootstrap/css/bootstrap.min.css')}} ">
	{{-- <link rel="stylesheet" type="text/css" href=" {{asset('assets/css/styles.css')}} "> --}}
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
	<!-- Paquete de iconos FA y Iconify -->
	<!-- Paquete de iconos FA y Iconify -->
	<script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
	<script src="https://kit.fontawesome.com/09fda739b2.js" crossorigin="anonymous"></script>	
</head>
<body>
@yield('css_extra')

@yield('content')
	</div>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
</body>
</html>    
