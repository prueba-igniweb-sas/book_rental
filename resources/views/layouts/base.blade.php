<!doctype html>
	<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href=" {{asset('assets/bootstrap/css/bootstrap.min.css')}} ">
		<link href='https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="{{asset('assets/css/dash/reset.css')}}"> <!-- CSS reset -->
		<link rel="stylesheet" href="{{asset('assets/css/dash/style1.css')}}"> <!-- Resource style -->
		<script src="{{asset('assets/js/dash/modernizr.js')}}"></script> <!-- Modernizr -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
		<script src="https://kit.fontawesome.com/09fda739b2.js" crossorigin="anonymous"></script>	
		  
		<title>Libreria</title>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" style="background-color: rgba(73, 41, 5); color:bisque;" id="navbarText">
				  <ul class="navbar-nav mr-auto">
					<li class="nav-item active" style="color:bisque;">
					  <a class="nav-link"  href="#"><span class="sr-only">(current)</span></a>
					</li>
					@can('all')
					<li class="nav-item" style="color:bisque;">
					  <a class="nav-link" href="{{url('categories')}}" style="color:bisque;">Categorias</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="{{url('clients')}}" style="color:bisque;">Clientes</a>
					</li>
					@endcan
				  </ul>
				  <span class="navbar-text" style="color:bisque;">
					Bienvenid@ {{Auth::user()->name}}       .
				  </span>
				</div>
			  </nav>
		
			<nav class="cd-stretchy-nav">
				<a class="cd-nav-trigger" href="#0">
					<span aria-hidden="true"></span>
				</a>
		
				<ul>
					{{-- <i class="fa fa-user-circle-o" aria-hidden="true"></i> --}}
					<li ><a href="{{route('home')}}" class="active"><span>Inicio</span></a></li>
					<li><a href="{{route('books')}}"><span >Libros</span></a></li>	
					{{-- @can('clients') --}}
					<li><a href="{{route('reservations')}}"><span>Reservaciones</span></a></li>
					{{-- @endcan --}}
					<li><a href="{{ route('logout') }}"
						onclick="event.preventDefault();
									  document.getElementById('logout-form').submit();">
						<span>Cerrar sesion</span><form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form></a>
					</li>
				</ul>
				<span aria-hidden="true" class="stretchy-nav-bg"></span>
			</nav>
		</header>
	<div style="margin: 2%;">
@yield('css_extra')

		@yield('content')
	</div>
	<main class="cd-main-content">
		<!-- main content here -->
	</main>
	
	{{-- script of login  --}}
	<script src="{{asset('assets/js/dash/jquery-2.1.4.js')}}"></script>
	<script src="{{asset('assets/js/dash/main.js')}}"></script>

	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	
	<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
	<script src=" {{asset('assets/bootstrap/js/bootstrap.min.js')}} "></script>
	<script src=" {{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@yield('js_extra')
</body>
</html>    
