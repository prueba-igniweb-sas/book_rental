@extends('layouts.base')

@section('css_extra')	
<style>
    #book_create
    {
        background-color:rgba(125, 201, 202 ,0.10);
        border-radius: 20px;
        margin-left: 15px;
    }
    h1{
        color: #492905;
        font-size: 40px;
    }
    #image_book{
        border-radius: 100%;
    }
    td{
        text-align: center;
    }

</style>
@endsection
@section('content')
@can('all')
<br>
<div class="title-page">
    <h1>Agregar un libro</h1>
</div>
<br>
<!-- Modal -->

@include('books.edit')
<form action="{{ route('books_store') }}" id="book_create" method="post" >
    @csrf
    <br>
    <div class="form-row" style="margin-left:15px; margin-right:15px;">
    <div class="col-3" id="button_image_block">
        <label for="name">Imagen de libro</label><br>
        <span class="text-danger error-text profile_picture_error"></span>
        <button type="button" class="browse_preview_profile btn btn-warning"> 
        <i class="fa fa-upload" style="color:#492905" aria-hidden="true"></i> Seleccionar imagen</button>
    </div>
    <div style="text-align: -webkit-center; display:none;" class="col-3" id="image_block">
        <img src=" {{asset('assets/images/profile_default.png')}} " id="preview_profile_client" class="browse_preview_profile" height="100px" width="100px" style="border-radius: 100%">
        <input type="file" name="book_image" class="file">
    </div>     
    <div class="col-3">
        <label for="name">Nombre</label>
        <input type="text" class="form-control" name="name">
        <span class="text-danger error-text name_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Categoria</label>
        <select name="category" class="form-control">
            <option selected disabled hidden>Seleccione</option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
        <span class="text-danger error-text category_error"></span>
    </div>
    <div class="col-3">
      <label for="name">Autor</label>
      <input type="text" class="form-control" name="author" >
      <span class="text-danger error-text author_error"></span>
    </div> 
    <div class="col-3">
        <label for="name">Editorial</label>
        <input type="text" class="form-control" name="editorial" >
        <span class="text-danger error-text editorial_error"></span>
    </div>
    <div class="col-2">
        <label for="name">Idioma</label>
        <input type="text" class="form-control" name="idiom">
        <span class="text-danger error-text idiom_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Fecha de lanzamiento</label>
        <input type="date" class="form-control" name="release_date">
        <span class="text-danger error-text release_date_error"></span>
    </div>
    <div class="col-3">
        <label for="name">Resumen</label>
        <textarea name="abstract" cols="30" rows="3"></textarea>
    </div>
     
    <div class="col-1">
    <br>
    <button type="submit" class="btn btn-secondary">Guardar libro</button>
    </div>
</div>
<br>
</form>
<br>
<br>
    <h1>Listado de libros</h1>
    <br>
        <div class="shadow mb-4">
            <div class="card-body">
                <table id="books_list" class="table table-hover table-codensed">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Imagen</th>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Autor</th>
                                <th style="text-align:center;">Editorial</th>
                                <th style="text-align:center;">idioma</th>
                                <th style="text-align:center;">Categoria</th>
                                <th style="text-align:center;">Abstract</th>
                                <th style="text-align:center;">Fecha de lanzamiento</th>
                                @can('all')
                                <th style="text-align:center;">Estado</th>
                                @endcan
                                <th style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                </table>		
            </div>
        </div>	
    </div>
</div>

@elsecan('clients')
<br>
<!-- Large modal -->
    <h1>Listado de libros </h1>
    
    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
    @endif
        <div class="shadow mb-4">
            <div class="card-body">
                <table id="books_list" class="table table-hover table-codensed" >
                        <thead style="background-color: #492905; color:aliceblue;">
                            <tr>
                                <th style="text-align:center;">Imagen</th>
                                <th style="text-align:center;">Nombre</th>
                                <th style="text-align:center;">Autor</th>
                                <th style="text-align:center;">idioma</th>
                                <th style="text-align:center;">Categoria</th>
                                <th style="text-align:center;">Fecha de lanzamiento</th>
                                <th style="text-align:center;">Estado</th>
                                <th style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>

                            @foreach ($c_books as $books)
                            <tbody>                            
                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                              <div class="card-header" id="headingOne">
                                                <h1 style="text-align: center">{{$books->name}}</h1>
                                                <h5 class="mb-0">
                                                  <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Datos del libro
                                                  </button>
                                                </h5>
                                              </div>
                                          
                                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">AUTOR: {{$books->author}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">EDITORIAL: {{$books->editorial}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">IDIOMA: {{$books->idiom}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">PUBLICACIÓN: {{$books->release_date}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>
                                              </div>
                                            </div>
                                            <div class="card">
                                              <div class="card-header" id="headingThree">
                                                <h5 class="mb-0">
                                                  <button class="btn btn-info collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    Resumen del libro
                                                  </button>
                                                </h5>
                                              </div>
                                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h5 class="card-title" style="color: #492905;">{{$books->abstract}}</h5></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                    </div>
                                </div>
                                
                                <td>
                                    <img id="image_book" src="{{asset('storage/book_images/'.$books->book_image.'')}}" width="50px;" height="50px" alt="">
                                </td>
                                <td>{{$books->name}}</td>
                                <td>{{$books->author}}</td>
                                <td>{{$books->idiom}}</td>
                                <td>{{$books->category->name}}</td>
                                <td>{{$books->release_date}}</td>
                                <td>                

                                    <h5><span class="badge badge-info">Disponible</span></h5></p>
                                </td>
                                <td><a href=""  data-toggle="modal" data-target=".bd-example-modal-lg">Ver</a> | <a href="" data-toggle="modal" data-target="#form_reservation">Reservar</a></td>
                                
                                <div class="modal fade" id="form_reservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Reservar: {{$books->name}}</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('reservation')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="book_id" value="{{$books->id}}">
                                                
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label for="">Días que desea reservar</label> 
                                                        <br><br>
                                                        <select name="rental_days" id="select-dates" class="form-control">
                                                            <option value="1">1 Día</option>
                                                            <option value="2">2 Días</option>
                                                            <option value="3">3 Días</option>
                                                            <option value="4">4 Días</option>
                                                            <option value="5">5 Días</option>
                                                            <option value="6">6 Días</option>
                                                            <option value="7">7 Días</option>
                                                            <option value="15">15 Días</option>
                                                            <option value="30">30 Días</option>
                                                        </select>
                                                    </div>
                                                    <br><br><br><br>
                                                    <div class="col-6">
                                                        <label for="">Fecha reserva</label> 
                                                        <br><br>
                                                        <input type="text" class="form-control" name="rental_date" id="rental_date" readonly>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="">Fecha entrega</label> 
                                                        <br><br>
                                                        <input type="text" class="form-control" name="date_of_delivery" id="date_of_delivery" readonly>
                                                    </div>
                                                
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                          <button type="submit" class="btn btn-success">Reservar</button>
                                        </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </tbody>          
                            @endforeach
                </table>		
            </div>
        </div>	
    </div>
</div>
    
@endcan
@endsection
@section('js_extra')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@can('all')
<script>
    // function to preview an image 
    $(function(){
        $(document).on("click", ".browse_preview_profile", function() {
            var file = $(this).parents().find(".file");
            file.trigger("click");
        });
        $('input[class="file"]').change(function(e) {
        var fileName = e.target.files[0].name;
        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            
            var image_profile_block = document.getElementById("image_block");
            var button_profile_block = document.getElementById("button_image_block");
            document.getElementById("preview_profile_client").src = e.target.result;

            if (image_profile_block.style.display === "none") {
                image_profile_block.style.display   = "block";
                button_profile_block.style.display  = "none";
            }else{
                image_profile_block.style.display   = "block";
            }
           
        };
        reader.readAsDataURL(this.files[0]);
    });

    });

</script>
 <!--SCRIPTS-->

{{-- start ajax functions  --}}
<script>
    toastr.options.preventDuplicates = true;

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });

    // function created of client 
    $(function(){
        $('#book_create').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType:'json',
                    beforeSend:function(){
                        $(form).find('span.error-text').text('');
                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $(form)[0].reset();
                            $('#books_list').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            console.log(1);
                        }
                    }
                });
            });
            // init table 
            $('#books_list').DataTable({
                language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Libros",
                "infoEmpty": "Mostrando 0 to 0 of 0 Productos",
                "infoFiltered": "(Filtrado de _MAX_ total Libros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Libros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                    }
                },
                processing:true,
                info:true,
                ajax:"{{url('books-get')}}",
                "pageLength":5,
                "aLengthMenu":[[5,10,25,50,-1],[5,10,25,50, "Todos"]],
                
                columns:[
                    {
                        data:'book_image', 
                        render:function(data,type, row)
                        {
                            return '<center><img src="storage/book_images/' + data +'"width="50" height="50"></center>';
                        }
                    },
                    {data:'name', name:'name'},
                    {data:'author', name:'author'},
                    {data:'editorial', name:'editorial'},
                    {data:'idiom', name:'idiom'},
                    {
                        data:'category.name', 
                        render:function(data,type, row)
                        {
                            return ''+data+'';
                        }
                    },
                    {data:'abstract', name:'abstract'},
                    {data:'release_date', name:'release_date'},
                    {
                        data:'status', 
                        render:function(data,type, row)
                        {
                            if (data === 1) {
                                return ' <h5><span class="badge badge-success">Disponible</span></h5>';
                            } else {
                                return ' <h5><span class="badge badge-warning">Alquilado</span></h5>';
                            }
                        }
                    },
                    {data:'actions', name:'actions', orderable:false, searchable:false},
                ]
                
                // seleccionar solo los registros de una paginacion 
            }).on('draw', function(){
                $('input[name="check_cliente"]').each(function(){this.checked = false;});
                $('input[name="main_checkbox"]').prop('checked', false);
                $('button#borrarTodo').addClass('d-none');
            });

            //edit book
            $(document).on('click', '#book_edit', function(){
                $('.edit_book_modal').modal('show');

                var book_id = $(this).data('id');
                $('.edit_book_modal').find('form')[0].reset();
                $('.edit_book_modal').find('span-error-text').text('');
               
                $.get('<?= url("books/'+book_id+'/edit") ?>',{book_id:book_id}, function(data){
                    $('.edit_book_modal').find('input[name="book_id"]').val(data.edit.id);
                    $('.edit_book_modal').find('input[name="name"]').val(data.edit.name);
                    $('.edit_book_modal').find('input[name="author"]').val(data.edit.author);
                    $('.edit_book_modal').find('input[name="editorial"]').val(data.edit.editorial);
                    $('.edit_book_modal').find('input[name="release_date"]').val(data.edit.release_date);
                    $('.edit_book_modal').find('input[name="idiom"]').val(data.edit.idiom);
                    var data_img = data.edit.book_image;
                    $('.edit_book_modal #imagen1').attr("src", "storage/book_images/"+data_img);
                    $('.edit_book_modal').find('textarea[name="abstract"]').val(data.edit.abstract);
                    $('.edit_book_modal').modal('show');
                },'json');
            });
                // actualizar producto  
                $('#book_form_edit').on('submit', function(e){
                e.preventDefault();
                var form = this;

                $.ajax({
                    url:$(form).attr('action'),
                    method:$(form).attr('method'),
                    data:new FormData(form),
                    processData:false,
                    dataType:'json',
                    contentType:false,
                    beforeSend: function(){

                    },
                    success:function(data){
                        if(data.code == 0){
                            $.each(data.error, function(prefix, val){
                                $(form).find('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                            $('#books_list').DataTable().ajax.reload(null, true);
                            $('.edit_book_modal').modal('hide');
                            $('.edit_book_modal').find('form')[0].reset();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    }

                })
            });
            // dele book
            $(document).on('click', '#book_destroy', function(){
                var book_id = $(this).data('id');
                // alert(producto_id);
                Swal.fire({
                    title:'¿Esta seguro?',
                    html:'Usted <b>borrará</b> este cliene de forma permanente ',
                    showCancelButton:true,
                    showCloseButton:true,
                    confirmButtonText:'Si, Borrar',
                    cancelButtonText:'Cancelar',
                    confirmButtonColor:'#556ee6',
                    cancelButtonColor:'#d33',
                    width:300,
                    allowOutsideClick:false
                }).then(function(result){
                    $.get('<?= url("book_delete/'+book_id+'") ?>', {book_id:book_id}, function(data){
                        if (data.code == 1) {
                            $('#books_list').DataTable().ajax.reload(null, true);
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }else if (data.code == 2) {
                            Swal.fire({
                                position: 'center',
                                icon: 'warning',
                                title: data.msg,
                                text: 'No puede eliminar un libro alquilado, espere a que lo devuelvan.',
                                showConfirmButton: false,
                                timer: 5000
                            });
                        }else{
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: data.msg,
                                showConfirmButton: false,
                                timer: 3000
                            });
                        }
                    },'json');
                });
            });
           
        // fin ajax funciones 
        });
            // fin table 
        // fin ajax funciones 
</script>    
@endcan
@can('clients')
<script>

    // function created of client 
    $(function(){
     $('#books_list').DataTable({
        "language": {
                    "search": "Buscar libros:",
                    "lengthMenu": "Mostrando _MENU_ libros por pagína.",
                    "zeroRecords": "Upss! Parece que aun no hay libros disponibles.",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "Sin libros disponibles.",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    }
                }
    });

    $("#select-dates").change(function () {
        $(this).find("option:selected")
                .each(function () {
            var dias = $(this).attr("value");
            let dia = parseInt(dias);
            let fechaEx = new Date();
            let fechaFactura = new Date();
            fechaEx.setDate(fechaEx.getDate() + dia);

            document.getElementById('rental_date').value = fechaFactura.toLocaleDateString();
            document.getElementById('date_of_delivery').value = fechaEx.toLocaleDateString();

        });
    }).change();
    });
    
    </script>
@endcan


