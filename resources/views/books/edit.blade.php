<!-- Modal -->
<div class="modal fade edit_book_modal" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar libro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="{{route('update_books')}}" method="post" id="book_form_edit">
                    @csrf
                    <input type="hidden" name="book_id" id="book_id">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-6">
                                <label for="">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text name_error"> </span>
                            </div>
                            <div class="col-6">
                                <label for="">Autor</label>
                                <input type="text" name="author" id="author" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text author_error"> </span>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-6">
                                <label for="">Editorial</label>
                                <input type="text" name="editorial" id="editorial" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger editorial_error"></span>
                            </div>
                            <div class="col-6">
                                <label for="">Idioma</label>
                                <input type="text" name="idiom" id="idiom" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text idiom_error"> </span>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-6">
                                <label for="">Fecha de lanzamiento</label>
                                <input type="date" name="release_date" id="release_date" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger release_date_error"></span>
                            </div>
                            <div class="col-6">
                                <label for="">Categoria</label>
                                <select name="category" id="category" class="form-control">
                                    <option disabled selected hidden>selecione</option>

                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger error-text abstract_error"> </span>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-6" style="text-align: -webkit-center;">
                                <label for="">Imagen de perfil</label>
                                <input type="file" name="book_image" id="imagen" class="form-control" placeholder="" aria-describedby="helpId">
                                <span class="text-danger error-text book_image_error"> </span>
                            </div>
                            <div class="col-6">
                                <br>
                                <img src="" id="imagen1" alt="" width="60px" height="60px">
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-12">
                                <label for="">Resumen</label> <br>
                                <textarea name="abstract" id="abstract" cols="40" rows="3"></textarea>
                                <span class="text-danger error-text abstract_error"> </span>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>