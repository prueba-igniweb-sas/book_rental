<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'name',
        'author',
        'editorial',
        'idiom',
        'abstract',
        'release_date',
        'status',
        'book_image'
    ];

    // relation to categories, relates a category to a book
    function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }

    // function that returns the data of a book in relation to a reservations 
    public function reservation()
    {
        return $this->hasMany(Reservations::class, 'book_id');
    } 
}
