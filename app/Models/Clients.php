<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'name',
        'lastname',
        'phone',
        'address',
        'profile_picture',
    ];

    // relation to users, Function that relates a client to a user.
    function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // function that returns the data of a client in relation to a reservations 
    public function reservation()
    {
        return $this->hasMany(Reservations::class, 'client_id');
    } 
  
}
