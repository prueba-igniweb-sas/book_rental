<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    use HasFactory;
    protected $fillable = [
        'book_id',
        'client_id',
        'name',
        'rental_days',
        'rental_date',
        'date_of_delivery',
        'active',
    ];

    // relation to books, Function that relates a reservations to a book.
    function book()
    {
        return $this->belongsTo(Books::class, 'book_id');
    }

    // relation to clients, Function that relates a reservations to a client.
    function client()
    {
        return $this->belongsTo(Clients::class, 'client_id');
    }
  
}
