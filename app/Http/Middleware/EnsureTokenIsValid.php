<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $get_token = $request->bearerToken();
        if ($get_token) {
            $token = Token::where('token', $get_token)->first();
            if ($token && $token->user) {
                if($token->user->hasRole(['Client', 'Administrator'])){
                    if ($token->valid_token()) {
                        Auth::login($token->user);
                        return $next($request);
                    }
                }
                return response()->json([
                    'message' => 'Permission denied.'
                ], 401);
                
            }
            return response()->json([
                'message' => 'Token does not exist.'
            ], 404);
        }
        return response()->json([
            'message' => 'Permission denied.'
        ], 401);
    }
}
