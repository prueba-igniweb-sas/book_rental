<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Categories;
use App\Models\Books;

class CategoriesController extends Controller
{
    // function create of paula 
    // function that returns the categories to the index view 
    public function index()
    {
        $categories = Categories::all();

        return view('categories.index')->with('categories', $categories);
    }

    // function create of paula 
    // function that returns the create view 
    public function create()
    {
        return view('categories.create');
    }

    // function create of paula 
    // function that creates a category
    public function store(Request $request){
       
        $validator = \Validator::make($request->all(),[
            'name'          => 'required',
            'description'   => 'required'
        ]);
        if(!$validator->passes()){
            return response()->json(['code'=>0,'error'=>$validator->errors()->toArray()]);
        }else{
        $category = new Categories();
        $category->name         = $request->get('name');
        $category->description  = $request->get('description');

        $query = $category->save();

        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo guardar la categoria!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'Categoria creado exitosame!']);
        }
        }
    }

    public function get_category(){
        $categories = Categories::all();

        return DataTables::of($categories)
        ->addIndexColumn()
        ->addColumn('Acciones', function($row){
            return ' 
            <div class="btn-group" >
                <button class="btn btn-sm btn-primary" style="text-align:center;" data-id="'.$row['id'].'" id="category_edit">Editar</button>
                <button class="btn btn-sm btn-danger" style="text-align:center;" data-id="'.$row['id'].'" id="category_destroy">Eliminar</button>
            </div>
            ';
        })
        
        ->rawColumns(['Acciones'])
        ->make(true);
    
    }

    // function create of paula 
    // function that sends data for editing
    public function edit($id)
    {
        $category = Categories::find($id);
       
        return response()->json(['edit' => $category]);
    }
   

    // function create of paula 
    // function that sends data for editing
    public function update_category(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name'          => 'required',
            'description'   => 'required',
        ]);

        if(!$validator->passes()){
            return response()->json(['code'=>0, 'error'=>$validator->errors()->toArray() ]);
        }
        else
        {
            $category = Categories::find($request->category_id);
            $category->name         = $request->get('name');
            $category->description  = $request->get('description');

            $query = $category->save();
            
            if(!$query){
                return response()->json(['code' => 0, 'msg'=>'¡No se pudo actualizar la categoria!']);
            }else{
                return response()->json(['code' => 1, 'msg'=>'¡Categoria actualizada exitosame!']);
            }
        }
    }
    public function destroy_category(Request $request, $id){
        $category   = Categories::find($id);
        $book       = Books::with('reservation')->where('category_id', $category->id)->first();

        if (!empty($book)) {
            if ($book->reservation()->exists()) {
                return response()->json(['code' => 2, 'msg'=>'¡No se puede eliminar la categoria, Hay libros alquilados con esta categoria.!']);
            }else{
                $book->delete();
            }
        }
        $query  = $category->delete(); 
     
        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo eliminar la categoria!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'¡Categoria eliminada exitosame!']);
        }
    }
}   
