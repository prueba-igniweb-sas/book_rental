<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Clients;
use App\Models\Books;
use App\Models\Reservations;
use App\Models\Categories;
use Illuminate\Support\Facades\Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // function that return a view home 
    public function index()
    {
        $clients        = Clients::count();
        $books          = Books::count();
        $reservations   = Reservations::count();
        $categories     = Categories::count();
        // client 
        $clientsC        =  Clients::where('user_id', Auth::user()->id)->first();
        if(!empty($clientsC)){
            $c_reservations =  Reservations::where('client_id', $clientsC->id)->count();
        }else{
            $c_reservations = 0;
        }
        
        $c_books    =  Books::where('status', 1)->count();

        return view('home', compact('clients', 'books', 'reservations', 'categories', 'c_reservations', 'c_books'));
    }
}
