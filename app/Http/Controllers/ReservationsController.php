<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Reservations;
use App\Models\books;
use App\Models\Clients;
use Carbon\Carbon;

class ReservationsController extends Controller
{
    // function that return on view reservations 
    public function index(){
        if(Auth::user()->hasRole(['client'])){
            $client = Clients::where('user_id', Auth::user()->id)->first();
            $reservations   = Reservations::with('book', 'client')->where('client_id', $client->pluck('id') )->get();
        }else{
            $reservations   = Reservations::with('book', 'client')->get();
        }
     
        $books          = Books::where('status', 1)->get();

        return view('reservations.index', compact('reservations', 'books'));
    }

    // function that returns a book
    public function book_devolution($id)
    {
        $reservation    = Reservations::find($id);
        $book           = Books::find($reservation->book_id);

        $book->status = 1;

        $reservation->delete();
        $book->save();

        return redirect()->route('reservations')->with('success', 'Se ha devuelto el libro con exito.');
    }

    // function that created on reservation 
    public function book_reservation(Request $request){
       
        $book       = Books::find($request->book_id);
        $client_id  = Clients::where('user_id', Auth::user()->id)->first();

        $reservation    = new Reservations();
        $reservation->book_id           = $request->book_id;
        $reservation->client_id         = $client_id->id;
        $reservation->name              = $book->name;
        $reservation->rental_days       = $request->rental_days;

        $formated   = $request->get('rental_date');
        $formated2  = $request->get('date_of_delivery');
        $formated_completed = Carbon::createFromFormat('d/m/Y', $formated)->toDateString();
        $formated_completed2 = Carbon::createFromFormat('d/m/Y', $formated2)->toDateString();

        $reservation->rental_date       = $formated_completed;
        $reservation->date_of_delivery  = $formated_completed2;
        $reservation->active            = 0;
        
        $book->status       = 2;

        $reservation->save();
        $book->save();

        return redirect()->route('reservations')->with('success', 'Ha reservado el libro "'.$book->name.'" con exito.');

    }
}
