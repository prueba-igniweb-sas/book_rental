<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use App\Models\Clients;
use App\Models\User;

class ClientsController extends Controller
{
    //  function created by paula 
    //  function that returns the clients to the index view 
    public function index()
    {
        $clients = Clients::all();

        return view('clients.index')->with('clients', $clients);
    }

    // function created by paula
    // function that creates customers
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name'      => 'required',
            'lastname'  => 'required',
            'email'     => 'email|required|unique:users|max:255',
            'phone'     => 'required',
            'address'   => 'required',
            'password'  => 'required|string|min:5'
            
        ]);
        if(!$validator->passes()){
            return response()->json(['code'=>0,'error'=>$validator->errors()->toArray()]);
        }else{
        $user = new User();
       
        $user->email    = $request->get('email');
        $user->name     = $request->get('name');
        $user->password = Hash::make($request->get('password'));

        $user->save();

        $user->assignRole('client');
        
        $client = new Clients();
        $client->user_id  = $user->id;
        $client->name     = $request->get('name');
        $client->lastname = $request->get('lastname');
        $client->phone    = $request->get('phone');
        $client->address  = $request->get('address');

        if($request->hasFile("profile_picture"))
        {
            $image          = $request->file("profile_picture");
            $path           = public_path("storage/profile_clients/");
            $image_name     = Str::slug($request->name).uniqid().'.'.$image->guessExtension();

            //$imagen->move($ruta,$nombreimagen);
            copy($image->getRealPath(),$path.$image_name);
            $client->profile_picture   = $image_name; 
        }
        $query = $client->save();
        
        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo guardar el cliente!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'¡Cliente creado exitosame!']);
        }
        }
        
    }
    public function get_clients(){
        $clients = Clients::with('users')->get();
    
        return DataTables::of($clients)
            ->addIndexColumn()
            ->addColumn('Acciones', function($row){
                return ' 
                <div class="btn-group">
                    <button class="btn btn-sm btn-primary" data-id="'.$row['id'].'" id="client_edit">Editar</button>
                    <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="client_destroy">Eliminar</button>
                </div>
                ';
            })
           
            ->rawColumns(['Acciones'])
            ->make(true);
    }

    // function that edit or client 
    public function edit($id){
        $client_id      = $id;
        $client_edit    = Clients::with('users')->where('id',$client_id)->first();

        return response()->json(['edit' => $client_edit]);
    }

    public function update_client(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name'      => 'required',
            'lastname'  => 'required',
            'phone'     => 'required',
            'address'   => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['code'=>0, 'error'=>$validator->errors()->toArray() ]);
        }else{
            $client_id  = $request->client_id;
            $client     = Clients::find($client_id);
            $client->name     = $request->get('name');
            $client->lastname = $request->get('lastname');
            $client->phone    = $request->get('phone');
            $client->address  = $request->get('address');
    
            if($request->hasFile("profile_picture"))
            {
                $image          = $request->file("profile_picture");
                $path           = public_path("storage/profile_clients/");
                $image_name     = Str::slug($request->name).uniqid().'.'.$image->guessExtension();
    
                //$imagen->move($ruta,$nombreimagen);
                copy($image->getRealPath(),$path.$image_name);
                $client->profile_picture   = $image_name; 
            }
            $user = User::where('id', $client->user_id)->first();
       
            $user->name     = $request->get('name');
    
            $user->save();
    
            $query = $client->save();
            
            if(!$query){
                return response()->json(['code' => 0, 'msg'=>'¡No se pudo actualizar el cliente!']);
            }else{
                return response()->json(['code' => 1, 'msg'=>'¡Cliente actualizado exitosame!']);
            }
        }
    }

    public function destroy_client(Request $request, $id){
        $client = Clients::find($id);
        $user   = User::find($client->user_id);

        $query1 = $client->delete();        
        $query = $user->delete();

        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo eliminar el cliente!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'¡Cliente eliminado exitosame!']);
        }
    }
    
}
