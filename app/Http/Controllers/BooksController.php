<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use App\Models\Books;
use App\Models\Categories;
use App\Models\Reservations;

class BooksController extends Controller
{
    // function create of paula 
    // function that returns the books to the index view 
    public function index()
    {
        $categories = Categories::all();
        $c_books = Books::where('status', 1)->get();

        return view('books.index', compact('categories', 'c_books'));
    }

    // function create of paula 
    // function that returns the categories to the create book view 
    public function create()
    {
        $categories = Categories::all();

        return view('books.create')->with('categories', $categories);
    }

    // function create of paula 
    // function that created on book 
    public function store(Request $request){
        $validator = \Validator::make($request->all(),[
            'category'   => 'required',
            'name'          => 'required',
            'author'        => 'required',
            'editorial'     => 'required',
            'idiom'         => 'required',
            'abstract'      => 'required',
            'release_date'  => 'required',
        ]);

        if(!$validator->passes()){
            return response()->json(['code'=>0, 'error'=>$validator->errors()->toArray() ]);
        }else{
            $book = new Books();
            $book->category_id  = $request->get('category');
            $book->name         = $request->get('name');
            $book->author       = $request->get('author');
            $book->editorial    = $request->get('editorial');
            $book->idiom        = $request->get('idiom');
            $book->abstract     = $request->get('abstract');
            $book->release_date = $request->get('release_date');
            if($request->hasFile("book_image"))
            {
                $image          = $request->file("book_image");
                $path           = public_path("storage/book_images/");
                $image_name     = Str::slug($request->name).uniqid().'.'.$image->guessExtension();
    
                //$imagen->move($ruta,$nombreimagen);
                copy($image->getRealPath(),$path.$image_name);
                $book->book_image   = $image_name; 
            }
            $query = $book->save();
            
        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo agregar el libro!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'¡Libro agregado exitosame!']);
        }

    }
    }
    // function create of paula
    // function that return books 
    public function get_books(){
        $books = Books::with('category')->get();

        return DataTables::of($books)
            ->addIndexColumn()
            ->addColumn('actions', function($row){
                return ' 
                <div class="btn-group">
                    <button class="btn btn-sm btn-primary" data-id="'.$row['id'].'" id="book_edit">Editar</button>
                    <button class="btn btn-sm btn-danger" data-id="'.$row['id'].'" id="book_destroy">Eliminar</button>
                </div>
                ';
            })
        
            ->rawColumns(['actions'])
            ->make(true);
    }

// function create of paula
    // function that edit or book 
    public function edit($id){
        $book_id    = $id;
        $books      = Books::with('category')->where('id',$book_id)->first();

        return response()->json(['edit' => $books]);
    }

    // function create of paula 
    // function that updates a book  
    public function update_book(Request $request){
        $validator = \Validator::make($request->all(),[
            'category'      => 'required',
            'name'          => 'required',
            'author'        => 'required',
            'editorial'     => 'required',
            'idiom'         => 'required',
            'abstract'      => 'required',
            'release_date'  => 'required',
        ]);

        if(!$validator->passes()){
            return response()->json(['code'=>0, 'error'=>$validator->errors()->toArray() ]);
        }else{
            $book = Books::find($request->book_id);
            $book->category_id  = $request->get('category');
            $book->name         = $request->get('name');
            $book->author       = $request->get('author');
            $book->editorial    = $request->get('editorial');
            $book->idiom        = $request->get('idiom');
            $book->abstract     = $request->get('abstract');
            $book->release_date = $request->get('release_date');
            if($request->hasFile("book_image"))
            {
                $image          = $request->file("book_image");
                $path           = public_path("storage/book_images/");
                $image_name     = Str::slug($request->name).uniqid().'.'.$image->guessExtension();
    
                //$imagen->move($ruta,$nombreimagen);
                copy($image->getRealPath(),$path.$image_name);
                $book->book_image   = $image_name; 
            }
            $query = $book->save();
            
        if(!$query){
            return response()->json(['code' => 0, 'msg'=>'¡No se pudo actualizar el libro!']);
        }else{
            return response()->json(['code' => 1, 'msg'=>'¡Libro actualizado exitosame!']);
        }

    }
    }

    // function that delete on book 
    public function destroy_book(Request $request, $id){
        $book           = Books::with('reservation')->find($id);

        if (!$book->reservation()->exists()) {
            $query = $book->delete();

            if(!$query){
                return response()->json(['code' => 0, 'msg'=>'¡No se pudo eliminar el libro!']);
            }else{
                return response()->json(['code' => 1, 'msg'=>'¡Libro eliminado exitosame!']);
            }
        }else{
                return response()->json(['code' => 2, 'msg'=>'¡El libro esta alquilado!']);
        }
    }
}
